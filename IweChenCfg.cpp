#include <boost/thread/mutex.hpp>

#include "IweChenCfg.h"
#include "JsonHelper.h"
#include "trace_worker.h"

static boost::mutex g_insMutexCalc;

IWeChenCfg::IWeChenCfg(const std::string &cfgFile)
{   trace_worker();
    trace_printf("cfgFile.c_str()  %s", cfgFile.c_str());
    printf("cfgFile.c_str()  %s\n", cfgFile.c_str());
    std::ifstream ifs(cfgFile);
    rapidjson::IStreamWrapper isw(ifs);
    if (m_cfggDocument.ParseStream(isw).HasParseError())
    {
        printf("GetParseError %d  %s\n", m_cfggDocument.GetParseError(), __FUNCTION__);
        return;
    }
}

IWeChenCfg::~IWeChenCfg()
{}


CWeChenCfg* CWeChenCfg::_instance = NULL;
CWeChenCfg* CWeChenCfg::instance() 
{	
	if (NULL == _instance)
	{
		boost::unique_lock<boost::mutex> guardMutex(g_insMutexCalc);
		if (NULL == _instance)
		{
			_instance = new CWeChenCfg("config.json");
		}
	}
	return _instance;
}

CWeChenCfg::CWeChenCfg(const std::string &cfgFile)
:IWeChenCfg(cfgFile)
{
}

CWeChenCfg::~CWeChenCfg()
{}

const rapidjson::Value &CWeChenCfg::Cfg()
{   trace_worker();
    return m_cfggDocument;
}

const rapidjson::Value &CWeChenCfg::AppIdCfg(const std::string &appId)
{   trace_worker();    
    return m_cfggDocument["APPID"][appId.c_str()];
}

const char *CWeChenCfg::GetSecret(const std::string &appId)
{   trace_worker();
    if (!m_cfggDocument.HasMember("APPID") 
        || !m_cfggDocument["APPID"].HasMember(appId.c_str()))
    {
        return "";
    }
    return m_cfggDocument["APPID"][appId.c_str()]["secret"].GetString();
}

const char *CWeChenCfg::GetAppIdTemp(const std::string &appId, const std::string &tempName)
{   trace_worker();    
    if (!m_cfggDocument.HasMember("APPID") 
        || !m_cfggDocument["APPID"].HasMember(appId.c_str()))
    {
        return "";
    }

    return m_cfggDocument["APPID"][appId.c_str()]["template"][tempName.c_str()].GetString();
}


