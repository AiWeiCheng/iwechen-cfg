#ifndef _IWECHEN_CFG_H
#define _IWECHEN_CFG_H
#include <rapidjson/document.h>


class IWeChenCfg
{
public:
    IWeChenCfg(const std::string &cfgFile);
    ~IWeChenCfg();
protected:    
    rapidjson::Document m_cfggDocument;
};


class CWeChenCfg: public IWeChenCfg
{
public:
    CWeChenCfg(const std::string &cfgFile);
    ~CWeChenCfg();
public:
    static CWeChenCfg* instance();
public:
    const char *GetSecret(const std::string &appId);
    const char *GetAppIdTemp(const std::string &appId, const std::string &tempName);
    const rapidjson::Value &AppIdCfg(const std::string &appId);
    const rapidjson::Value &Cfg();
private:
    static	CWeChenCfg* _instance;
};

#endif

